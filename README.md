# Quasar App (vue-quasar-base)

Base configured project scaffold for Quasar apps

## Getting started

### Environment

#### VS Code
* Install VS Code
* Install Settings Sync extension https://marketplace.visualstudio.com/items?itemName=Shan.code-settings-sync
* Configure it to use this Gist https://gist.github.com/david-katona/ee54de0f47a6bc34ed73637e33a8a26d
  * Or download the settings manually.
* Let VS Code install Remote - Containers extension

#### Project
* Add a new remote pointing to this project: `git remote add quasar-base git@gitlab.com:katona.david/vue-quasar-base.git`
* Add this project to your new project as a git subtree `git subtree add -P frontend quasar-base master --squash` 
* To update the base quasar project run `git subtree pull -P frontend quasar-base master --squash`

#### User- and project specific changes
* User-specific data is marked with `!CUSTOM USER` and project-specific data is marked with `!CUSTOM PROJECT`
* Search for `!CUSTOM` in files. Change the data (if required) and remove the `!CUSTOM ...` comment.

## Install the dependencies
```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
yarn run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
