export interface ConfigState {
  _isDarkMode: boolean;
  _isLeftDrawerOpen: boolean;
}
