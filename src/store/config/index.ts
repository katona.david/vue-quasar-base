import { ActionTree, GetterTree } from 'vuex/types';
import { VuexModule } from 'vuex-typed-modules';

import { Dark } from 'quasar';

import { ConfigState } from './config.types';

/*eslint @typescript-eslint/no-use-before-define: ["error", { "variables": false }]*/
const actions: ActionTree<ConfigState, {}> = {
  applyGlobalConfig() {
    // Setting up $store.watch() for these state variables would be nicer however it's very verbose and introduces additional watchers.
    ConfigStore.actions.setDarkMode(ConfigStore.getters.isDarkMode);
  },
  setDarkMode(context, value: boolean) {
    ConfigStore.mutations._setDarkMode(value);
    Dark.set(value);
  },
  setLeftDrawerOpen(context, value: boolean) {
    ConfigStore.mutations._setLeftDrawerOpen(value);
  },
  toggleLeftDrawer() {
    ConfigStore.mutations._toggleLeftDrawer();
  },
};

const getters: GetterTree<ConfigState, {}> = {
  isLeftDrawerOpen(state) {
    return state._isLeftDrawerOpen;
  },
  isDarkMode(state) {
    return state._isDarkMode;
  },
};

const mutations = {
  _setDarkMode(state: ConfigState, payload: boolean) {
    state._isDarkMode = payload;
  },
  _setLeftDrawerOpen(state: ConfigState, payload: boolean) {
    state._isLeftDrawerOpen = payload;
  },
  _toggleLeftDrawer(state: ConfigState) {
    state._isLeftDrawerOpen = !state._isLeftDrawerOpen;
  },
};

const state: ConfigState = {
  _isDarkMode: true,
  _isLeftDrawerOpen: false,
};

const ConfigStore = new VuexModule({
  name: 'config',
  actions,
  getters,
  mutations,
  state,
});

export default ConfigStore;
