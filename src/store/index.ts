import Vue from 'vue';
import Vuex from 'vuex';

import { Database } from 'vuex-typed-modules';
import VuexPersistence from 'vuex-persist';

import config from './config';


Vue.use(Vuex);

export interface StoreInterface {
  // Define your own store structure, using submodules if needed
  // example: ExampleStateInterface;
  example: unknown; // Declared as unknown to avoid linting issue. Best to strongly type as per the line above.
}

const isDevMode: boolean = process.env.NODE_ENV === 'development';

const vuexLocalPersistence = new VuexPersistence<{}>({
  storage: window.localStorage,
  strictMode: isDevMode,
});

const database = new Database({ logger: isDevMode });


export default new Vuex.Store<{}>({
  mutations: {
    RESTORE_MUTATION: vuexLocalPersistence.RESTORE_MUTATION, // Needed for strict mode support
  },
  plugins: [database.deploy([config]), vuexLocalPersistence.plugin],
  strict: isDevMode,
});
